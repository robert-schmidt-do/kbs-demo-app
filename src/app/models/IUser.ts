export interface IUser {
  email: string;
  password: string;
  role: ERole;
}

export enum ERole {
  admin,
  editor,
  user
}


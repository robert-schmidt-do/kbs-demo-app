import {Component, OnDestroy, OnInit} from '@angular/core';
import { Store } from '@ngxs/store';
import { Router } from '@angular/router';
import { Logout } from '../state/auth/auth.actions';
import { IItem } from '../models/IItem';
import { GetAllItems } from '../state/item/item.actions';
import { ItemState } from '../state/item/item.state';
import {Observable} from "rxjs";

@Component({
  selector: 'article-list',
  templateUrl: 'article-list.page.html',
  styleUrls: ['article-list.page.scss'],
})
export class ArticleListPage implements OnInit {
  private items$: Observable<IItem[]>;

  constructor(private store: Store, private router: Router) {
  }

  public ngOnInit(): void {
    this.store.dispatch(new GetAllItems());
    this.items$ = this.store.select(ItemState.getItems);
  }

  public logout(): void {
    this.store.dispatch(new Logout()).subscribe(() => {
      this.router.navigate(['/login']);
    });
  }
}

import { Component } from '@angular/core';
import { LoginWithPassword } from '../state/auth/auth.actions';
import { Store } from '@ngxs/store';
import { Router } from '@angular/router';
import { ERole } from '../models/IUser';

@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
})
export class LoginPage {
   public email = '';
   public password = '';

  constructor(private store: Store, private router: Router) { }

  public login(): void {
    this.store.dispatch(new LoginWithPassword(this.email, this.password, ERole.admin)).subscribe(() => {
      this.router.navigate(['/article-list']);
    });
  }
}

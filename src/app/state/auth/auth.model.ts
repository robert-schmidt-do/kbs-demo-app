import { IUser } from 'src/app/models/IUser';

export interface IAuthState {
  user: IUser;
  token: string;
}

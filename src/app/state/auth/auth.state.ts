import { Action, Selector, StateContext, State } from '@ngxs/store';
import { IUser } from 'src/app/models/IUser';
import { LoginWithPassword, Logout } from './auth.actions';
import { IAuthState } from './auth.model';
import { AuthService } from 'src/app/services/auth.service';
import { tap } from 'rxjs/operators';

@State<IAuthState>({
  name: 'auth',
  defaults: {
    user: null,
    token: ''
  }
})

export class AuthState {
  constructor(private authService: AuthService) { }

  @Selector()
  static getCurrentUser(state: IUser) {
    return state.email;
  }

  @Action(LoginWithPassword)
  loginWithPassword({ getState, patchState }: StateContext<IAuthState>, action: LoginWithPassword) {
    return this.authService.login(action.email, action.password, action.role).pipe(tap(result => {
      patchState({ user: { email: action.email, password: action.password, role: action.role }, token: result });
    }));
  }

  @Action(Logout)
  logout({ getState, patchState }: StateContext<IAuthState>, action: Logout) {
    return patchState({ user: null, token: '' });
  }
}

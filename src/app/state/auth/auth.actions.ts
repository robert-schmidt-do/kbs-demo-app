import { ERole } from 'src/app/models/IUser';

export class LoginWithPassword {
  static readonly type = '[auth] login with password';
  constructor(public email: string, public password: string, public role: ERole) { }
}

export class Logout {
  static readonly type = '[auth] logout';
}

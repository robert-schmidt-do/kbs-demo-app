import { Action, State, StateContext, Selector } from '@ngxs/store';
import { IItemState } from './item.model';
import { ItemService } from 'src/app/services/item.service';
import { GetAllItems } from './item.actions';
import { tap } from 'rxjs/operators';

@State<IItemState>({
  name: 'items',
  defaults: { items: [] }
})

export class ItemState {
  constructor(private itemService: ItemService) { }

  @Selector()
  static getItems(state: IItemState) {
    return state.items;
  }

  @Action(GetAllItems)
  getAllItems({ getState, patchState }: StateContext<IItemState>, action: GetAllItems) {
    return this.itemService.getItems().pipe(tap(result => {
      patchState({items: result});
    }));
  }
}

import { IItem } from 'src/app/models/IItem';

export interface IItemState {
  items: IItem[];
}

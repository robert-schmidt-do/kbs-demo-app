import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IItem } from '../models/IItem';

@Injectable()
export class ItemService {
  private items: IItem[] = [
    { title: 'item1', description: 'some lines of text to describe the article' },
    { title: 'item2', description: 'some lines of text to describe the article' },
    { title: 'item3', description: 'some lines of text to describe the article' },
    { title: 'item4', description: 'some lines of text to describe the article' },
    { title: 'item5', description: 'some lines of text to describe the article' }
  ];

  public getItems(): Observable<IItem[]> {
    return of(this.items);
  }
}

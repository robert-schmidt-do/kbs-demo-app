import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { IUser, ERole } from '../models/IUser';

@Injectable()
export class AuthService {
  private user: IUser = { email: 'test@mail.com', password: '123456', role: ERole.admin };
  private token = 'abcde';

  public login(email: string, password: string, role: ERole): Observable<string> {
    const loginData: IUser = { email, password, role };
    if (this.user.email === loginData.email && this.user.password === loginData.password && this.user.role === loginData.role) {
      return of(this.token);
    } else {
      return throwError('Wrong userdata, try test@mail.com and 123456 as password.');
    }
  }
}
